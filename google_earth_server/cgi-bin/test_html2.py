#!/usr/bin/python

import random
import pymysql.cursors

#print ("Content-type:text/html\r\n\r\n")
#print("<HTML><HEAD><TITLE>TEST</TITLE></HEAD><BODY><H1>IT WORKS!</H1>")

try:
    connection = pymysql.connect(host='127.0.0.1',
                                     port=3306,
                                     user='root',
                                     password='vagrant',
                                     db='python_fcc_db_test',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
    
    with connection.cursor() as cursor:
        sql = """
                select
                    st_x(tlo.lat_long_point) as lon_decimal,
                    st_y(tlo.lat_long_point) as lat_decimal,
                    tlo.ground_elevation as t_ground_elevation,
                    st_x(rlo.lat_long_point) as r_lon_decimal,
                    st_y(rlo.lat_long_point) as r_lat_decimal,
                    rlo.ground_elevation as r_ground_elevation,
                    CONCAT(mp.unique_system_identifier, '_', mp.callsign) as uis
                    from 
                        entities_of_interest as en,
                        microwave_paths as mp,
                        location as tlo,
                        location as rlo
                    where 
                        en.call_sign = mp.callsign
                        and tlo.call_sign = mp.callsign
                        and tlo.location_number = mp.transmit_location_number
                        and rlo.call_sign = mp.callsign
                        and rlo.location_number = mp.receiver_location_number
                        and tlo.lat_long_point IS NOT NULL
                        and rlo.lat_long_point IS NOT NULL
                    limit 10000
                """
                
        cursor.execute(sql)
        results = cursor.fetchall()
        kml = (
           '<?xml version="1.0" encoding="UTF-8"?>\n'
           '<kml xmlns="http://www.opengis.net/kml/2.2">\n'
           '<Document>\n'
        )
        # print(kml)
        
        print("Results: <ul>")
        for result in results:
            # kml = (' ')
            lat_dec = result['lat_decimal']
            lon_dec = result['lon_decimal'] # * -1
            
            
            r_lat_dec = result['r_lat_decimal']
            r_lon_dec = result['r_lon_decimal']
            
            t_ground_elevation = 0
            r_ground_elevation = 0
            
            try:
                t_ground_elevation = float(result['t_ground_elevation']) #/ 3.28 #3.28 feet per meter
                r_ground_elevation = float(result['r_ground_elevation']) #/ 3.28 #3.28 feet per meter
            except Exception as e:
                print("Exception but ok")
                pass
            uis = result['uis']
            kml += '<Placemark>\n'
            kml += '<name>%s</name>\n' % (uis)
        
            # kml += '<Point>\n'
            # kml += '<coordinates>%f,%f</coordinates>\n' % (lon_dec, lat_dec)
            # kml += '</Point>\n'
            kml += '<LineString><extrude>0</extrude><tesselate>1</tesselate>\n'
            kml += '<altitudeMode>absolute</altitudeMode>'
            kml += '<coordinates>%f,%f,%f %f,%f,%f</coordinates>\n' % (lon_dec, lat_dec, t_ground_elevation, r_lon_dec, r_lat_dec, r_ground_elevation)
            kml += '</LineString>\n'

            kml += '</Placemark>\n'
            # print(kml)
            
            
            # print("<li>%f,%f</li>" % (lat_dec, lon_dec))

        print ("Content-Type: application/vnd.google-earth.kml+xml\n")
        print(kml)
        print("</Document></kml>")
        # print("</ul>")
        
except  Exception as error:
    print(error)
finally:
    connection.close()

#print("</BODY></HTML>")

