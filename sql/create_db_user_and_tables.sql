
select 'Dropping python_fcc_db_test if it already exists and then creating' as '';

drop database if exists python_fcc_db_test;
create database python_fcc_db_test;
use python_fcc_db_test;

/*
drop user if exists 'root'@'%';
create user 'root'@'%' identified by 'Password123!';
grant all privileges on *.* to 'root'@'%';
*/
/*
show tables;
*/

select 'Dropping and then recreating python_fcc_db user' as '';
drop user if exists python_fcc_db;
SET GLOBAL validate_password.policy=LOW;
FLUSH PRIVILEGES;
drop user if exists 'python_fcc_db'@'localhost';
create user 'python_fcc_db'@'localhost' identified by 'python_fcc_db';


select 'Enabling global_local_infile to ON and disabling SQL_SAFE_UPDATES' as '';
/*show variables like 'local_infile';*/
set global local_infile = 'ON';
SET SQL_SAFE_UPDATES=0; /*Allows updates without the where clause for example*/


/**
 * Begin creating and load FCC database Tables
 */

select 'Creating entities table' as '';
drop table if exists entities;
create table entities (
	record_type char(2) NOT NULL,
	unique_system_identifier numeric(9,0) NOT NULL,
	uls_file_number char(14) NULL,
	ebf_number varchar(30) NULL,
	call_sign char(10) NULL,
	entity_type char(2) NULL,
	licensee_id char(9) NULL,
	entity_name varchar(200) NULL,
	first_name varchar(20) NULL,
	mi char(1) NULL,
	last_name varchar(20) NULL,
	suffix char(3) NULL,
	phone char(10) NULL,
	fax char(10) NULL,
	email varchar(50) NULL,
	street_address varchar(60) NULL,
	city varchar(20) NULL,
	state char(2) NULL,
	zip_code char(9) NULL,
	po_box varchar(20) NULL,
	attention_line varchar(35) NULL,
	sgin char(3) NULL,
	frn char(10) NULL,
	applicant_type_code char(1) NULL,
	applicant_type_other char(40) NULL,
	status_code char(1) NULL,
	status_date varchar(255) NULL
);



select 'Loading entities table from /vagrant/data/EN.dat' as '';
load data infile '/vagrant/data/EN.dat'
into table entities fields terminated by '|' ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@call_sign,@entity_type,@licensee_id,@entity_name,@first_name,@mi,@last_name,@suffix,@phone,@fax,@email,@street_address,@city,@state,@zip_code,@po_box,@attention_line,@sgin,@frn,@applicant_type_code,@applicant_type_other,@status_code,@status_date) 
set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
call_sign = if(@call_sign='', null, @call_sign),
entity_type = if(@entity_type='', null, @entity_type),
licensee_id = if(@licensee_id='', null, @licensee_id),
entity_name = if(@entity_name='', null, @entity_name),
first_name = if(@first_name='', null, @first_name),
mi = if(@mi='', null, @mi),
last_name = if(@last_name='', null, @last_name),
suffix = if(@suffix='', null, @suffix),
phone = if(@phone='', null, @phone),
fax = if(@fax='', null, @fax),
email = if(@email='', null, @email),
street_address = if(@street_address='', null, @street_address),
city = if(@city='', null, @city),
state = if(@state='', null, @state),
zip_code = if(@zip_code='', null, @zip_code),
po_box = if(@po_box='', null, @po_box),
attention_line = if(@attention_line='', null, @attention_line),
sgin = if(@sgin='', null, @sgin),
frn = if(@frn='', null, @frn),
applicant_type_code = if(@applicant_type_code='', null, @applicant_type_code),
applicant_type_other = if(@applicant_type_other='', null, @applicant_type_other),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date)
;



select 'Dropping and recreating location table' as '';
drop table if exists location;
create table location
(
      record_type               char(2)              not null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                 char(10)             null,
      location_action_performed char(1)              null,
      location_type_code        char(1)              null,
      location_class_code       char(1)              null,
      location_number           int                  null,
      site_status               char(1)              null,
      corresponding_fixed_location int               null,
      location_address          varchar(80)          null,
      location_city             char(20)             null,
      location_county           varchar(60)          null,
      location_state            char(2)              null,
      radius_of_operation       numeric(5,1)         null,
      area_of_operation_code    char(1)              null,
      clearance_indicator       char(1)              null,
      ground_elevation          numeric(7,1)         null,
      lat_degrees               int                  null,
      lat_minutes               int                  null,
      lat_seconds               numeric(3,1)         null,
      lat_direction             char(1)              null,
      long_degrees              int                  null,
      long_minutes              int                  null,
      long_seconds              numeric(3,1)         null,
      long_direction            char(1)              null,
      max_lat_degrees           int                  null,
      max_lat_minutes           int                  null,
      max_lat_seconds           numeric(3,1)         null,
      max_lat_direction         char(1)              null,
      max_long_degrees          int                  null,
      max_long_minutes          int                  null,
      max_long_seconds          numeric(3,1)         null,
      max_long_direction        char(1)              null,
      nepa                      char(1)              null,
      quiet_zone_notification_date char(10)          null,
      tower_registration_number char(10)             null,
      height_of_support_structure numeric(7,1)       null,
      overall_height_of_structure numeric(7,1)       null,
      structure_type            char(7)              null,
      airport_id                char(4)              null,
      location_name             char(20)             null,
      units_hand_held           int                  null,
      units_mobile              int                  null,
      units_temp_fixed          int                  null,
      units_aircraft            int                  null,
      units_itinerant           int                  null,
      status_code		char(1)		     null,
      status_date		varchar(255) null,
      earth_agree               char(1)              null
);



select 'Loading location table from /vagrant/data/LO.dat' as '';
load data infile '/vagrant/data/LO.dat'
into table location fields terminated by '|'  ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@call_sign,@location_action_performed,@location_type_code,@location_class_code,@location_number,@site_status,@corresponding_fixed_location,@location_address,@location_city,@location_county,@location_state,@radius_of_operation,@area_of_operation_code,@clearance_indicator,@ground_elevation,@lat_degrees,@lat_minutes,@lat_seconds,@lat_direction,@long_degrees,@long_minutes,@long_seconds,@long_direction,@max_lat_degrees,@max_lat_minutes,@max_lat_seconds,@max_lat_direction,@max_long_degrees,@max_long_minutes,@max_long_seconds,@max_long_direction,@nepa,@quiet_zone_notification_date,@tower_registration_number,@height_of_support_structure,@overall_height_of_structure,@structure_type,@airport_id,@location_name,@units_hand_held,@units_mobile,@units_temp_fixed,@units_aircraft,@units_itinerant,@status_code,@status_date,@earth_agree) set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
call_sign = if(@call_sign='', null, @call_sign),
location_action_performed = if(@location_action_performed='', null, @location_action_performed),
location_type_code = if(@location_type_code='', null, @location_type_code),
location_class_code = if(@location_class_code='', null, @location_class_code),
location_number = if(@location_number='', null, @location_number),
site_status = if(@site_status='', null, @site_status),
corresponding_fixed_location = if(@corresponding_fixed_location='', null, @corresponding_fixed_location),
location_address = if(@location_address='', null, @location_address),
location_city = if(@location_city='', null, @location_city),
location_county = if(@location_county='', null, @location_county),
location_state = if(@location_state='', null, @location_state),
radius_of_operation = if(@radius_of_operation='', null, @radius_of_operation),
area_of_operation_code = if(@area_of_operation_code='', null, @area_of_operation_code),
clearance_indicator = if(@clearance_indicator='', null, @clearance_indicator),
ground_elevation = if(@ground_elevation='', 0.0, @ground_elevation),
lat_degrees = if(@lat_degrees='', null, @lat_degrees),
lat_minutes = if(@lat_minutes='', null, @lat_minutes),
lat_seconds = if(@lat_seconds='', null, @lat_seconds),
lat_direction = if(@lat_direction='', null, @lat_direction),
long_degrees = if(@long_degrees='', null, @long_degrees),
long_minutes = if(@long_minutes='', null, @long_minutes),
long_seconds = if(@long_seconds='', null, @long_seconds),
long_direction = if(@long_direction='', null, @long_direction),
max_lat_degrees = if(@max_lat_degrees='', null, @max_lat_degrees),
max_lat_minutes = if(@max_lat_minutes='', null, @max_lat_minutes),
max_lat_seconds = if(@max_lat_seconds='', null, @max_lat_seconds),
max_lat_direction = if(@max_lat_direction='', null, @max_lat_direction),
max_long_degrees = if(@max_long_degrees='', null, @max_long_degrees),
max_long_minutes = if(@max_long_minutes='', null, @max_long_minutes),
max_long_seconds = if(@max_long_seconds='', null, @max_long_seconds),
max_long_direction = if(@max_long_direction='', null, @max_long_direction),
nepa = if(@nepa='', null, @nepa),
quiet_zone_notification_date = if(@quiet_zone_notification_date='', null, @quiet_zone_notification_date),
tower_registration_number = if(@tower_registration_number='', null, @tower_registration_number),
height_of_support_structure = if(@height_of_support_structure='', null, @height_of_support_structure),
overall_height_of_structure = if(@overall_height_of_structure='', null, @overall_height_of_structure),
structure_type = if(@structure_type='', null, @structure_type),
airport_id = if(@airport_id='', null, @airport_id),
location_name = if(@location_name='', null, @location_name),
units_hand_held = if(@units_hand_held='', null, @units_hand_held),
units_mobile = if(@units_mobile='', null, @units_mobile),
units_temp_fixed = if(@units_temp_fixed='', null, @units_temp_fixed),
units_aircraft = if(@units_aircraft='', null, @units_aircraft),
units_itinerant = if(@units_itinerant='', null, @units_itinerant),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date),
earth_agree = if(@earth_agree='', null, @earth_agree)
;

select 'Adding index to (call_sign, location_number) on location table' as '';
alter table location add index callsign_location_num_index(call_sign, location_number) USING BTREE ; /* use BTREE instead of default HASH, else queries only requiring just callsign wont be able to use index */

select 'Adding column lat_long_point and setting its value on location table' as '';
alter table location add column lat_long_point POINT;
update location 
	set 
		lat_long_point = POINT(
			IF(long_direction='W',-1.0,1.0) * (long_degrees + (long_minutes/60.0) + (long_seconds/3600.0)),
			IF(lat_direction ='S',-1.0,1.0) * (lat_degrees + (lat_minutes/60.0) + (lat_seconds/3600.0))
            );



select 'Dropping and recreating microwave_paths table' as '';
drop table if exists microwave_paths;
create table microwave_paths
(
      record_type               char(2)              null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      callsign                  char(10)             null,
      path_action_performed     char(1)              null,
      path_number               int                  null,
      transmit_location_number  int                  null,
      transmit_antenna_number   int                  null,
      receiver_location_number  int                  null,
      receiver_antenna_number   int                  null,
      mas_dems_subtype          char(2)              null,
      path_type_desc            char(20)             null,
      passive_receiver_indicator char(1)             null,
      country_code              char(3)              null,
      interference_to_gso       char(1)              null,
      receiver_callsign         varchar(10)          null,
      angular_sep		numeric(3,2)         null,
      cert_no_alternative	char(1)		     null,
      cert_no_interference	char(1)  	     null,
	status_code		char(1)			null,
	status_date		varchar(255)		null
);


select 'Loading microwave_paths table from /vagrant/data/PA.dat' as '';
load data infile '/vagrant/data/PA.dat'
into table microwave_paths fields terminated by '|' ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@callsign,@path_action_performed,@path_number,@transmit_location_number,@transmit_antenna_number,@receiver_location_number,@receiver_antenna_number,@mas_dems_subtype,@path_type_desc,@passive_receiver_indicator,@country_code,@interference_to_gso,@receiver_callsign,@angular_sep,@cert_no_alternative,@cert_no_interference,@status_code,@status_date) 
set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
callsign = if(@callsign='', null, @callsign),
path_action_performed = if(@path_action_performed='', null, @path_action_performed),
path_number = if(@path_number='', null, @path_number),
transmit_location_number = if(@transmit_location_number='', null, @transmit_location_number),
transmit_antenna_number = if(@transmit_antenna_number='', null, @transmit_antenna_number),
receiver_location_number = if(@receiver_location_number='', null, @receiver_location_number),
receiver_antenna_number = if(@receiver_antenna_number='', null, @receiver_antenna_number),
mas_dems_subtype = if(@mas_dems_subtype='', null, @mas_dems_subtype),
path_type_desc = if(@path_type_desc='', null, @path_type_desc),
passive_receiver_indicator = if(@passive_receiver_indicator='', null, @passive_receiver_indicator),
country_code = if(@country_code='', null, @country_code),
interference_to_gso = if(@interference_to_gso='', null, @interference_to_gso),
receiver_callsign = if(@receiver_callsign='', null, @receiver_callsign),
angular_sep = if(@angular_sep='', null, @angular_sep),
cert_no_alternative = if(@cert_no_alternative='', null, @cert_no_alternative),
cert_no_interference = if(@cert_no_interference='', null, @cert_no_interference),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date)
;

select 'Adding index (callsign, transmit_location_number) to microwave_paths table' as '';
alter table microwave_paths add index callsign_index(callsign, transmit_location_number) USING BTREE ; /* use BTREE instead of default HASH, else queries only requiring just callsign wont be able to use index */




drop table if exists leased_microwave_paths; 
create table leased_microwave_paths
(
      record_type               char(2)              null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      callsign                  char(10)             null,
      lease_id                 char(10)              null, 
      ls_site_link_id           numeric(9,0)         null, 
      path_action_performed     char(1)              null,
      path_number               int                  null,
      transmit_location_number  int                  null,
      transmit_antenna_number   int                  null,
      receiver_location_number  int                  null,
      receiver_antenna_number   int                  null,
      mas_dems_subtype          char(2)              null,
      path_type_desc            char(20)             null,
      passive_receiver_indicator char(1)             null,
      country_code              char(3)              null,
      interference_to_gso       char(1)              null,
      receiver_callsign         varchar(10)          null,
      angular_sep		numeric(3,2)         null,
      cert_no_alternative	char(1)		     null,
      cert_no_interference	char(1)  	     null,
	status_code		char(1)			null,
	status_date		varchar(255)		null
);

load data infile '/vagrant/data/p2.dat'
into table leased_microwave_paths fields terminated by '|' ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@callsign,@lease_id,@ls_site_link_id,
@path_action_performed,@path_number,@transmit_location_number,@transmit_antenna_number,@receiver_location_number,@receiver_antenna_number,
@mas_dems_subtype,@path_type_desc,@passive_receiver_indicator,@country_code,@interference_to_gso,@receiver_callsign,
@angular_sep,@cert_no_alternative,@cert_no_interference,@status_code,@status_date) 
set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
callsign = if(@callsign='', null, @callsign),
path_action_performed = if(@path_action_performed='', null, @path_action_performed),
path_number = if(@path_number='', null, @path_number),
transmit_location_number = if(@transmit_location_number='', null, @transmit_location_number),
transmit_antenna_number = if(@transmit_antenna_number='', null, @transmit_antenna_number),
receiver_location_number = if(@receiver_location_number='', null, @receiver_location_number),
receiver_antenna_number = if(@receiver_antenna_number='', null, @receiver_antenna_number),
mas_dems_subtype = if(@mas_dems_subtype='', null, @mas_dems_subtype),
path_type_desc = if(@path_type_desc='', null, @path_type_desc),
passive_receiver_indicator = if(@passive_receiver_indicator='', null, @passive_receiver_indicator),
country_code = if(@country_code='', null, @country_code),
interference_to_gso = if(@interference_to_gso='', null, @interference_to_gso),
receiver_callsign = if(@receiver_callsign='', null, @receiver_callsign),
angular_sep = if(@angular_sep='', null, @angular_sep),
cert_no_alternative = if(@cert_no_alternative='', null, @cert_no_alternative),
cert_no_interference = if(@cert_no_interference='', null, @cert_no_interference),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date)
;


/*
select count(*) from entities where 
	email like '%mckay-brothers.com'
OR email like '%wcwtech.com'
OR email like '%geodesicnetworks.com'
OR email like '%auburndata.com'
OR email like '%abservicesllc.com'
OR email like '%NeXXComwireless.com'
OR email like '%isignalnetworks.com'
OR email like '%anova-tech.com'
OR email like '%infiniumcm.com'
OR email like '%tatora.com'
OR email like '%midwestics.com'
OR email like '%apsaranetworks.com'
OR email like '%bsonetwork.com'
OR email like '%striketechnologies.com'
OR email like '%akingump.com'
OR email like '%surveillancetechs.com'
OR email like '%bobbroadband.com'
OR email like '%gammafcc@gmail.com'
OR email like '%newlinenet.com'
    ;
    
*/

select 'Dropping and recreating entities_of_interest table' as '';
drop table if exists entities_of_interest;
create table entities_of_interest as select * from entities where 
	email like '%mckay-brothers.com'
	OR email like '%wcwtech.com'
	OR email like '%geodesicnetworks.com'
	OR email like '%auburndata.com'
	OR email like '%abservicesllc.com'
	OR email like '%NeXXComwireless.com'
	OR email like '%isignalnetworks.com'
	OR email like '%anova-tech.com'
	OR email like '%infiniumcm.com'
	OR email like '%tatora.com'
	OR email like '%midwestics.com'
	OR email like '%apsaranetworks.com'
	OR email like '%bsonetwork.com'
	OR email like '%striketechnologies.com'
	OR email like '%akingump.com'
	OR email like '%surveillancetechs.com'
	OR email like '%bobbroadband.com'
	OR email like '%gammafcc@gmail.com' 
	OR email like '%newlinenet.com'  
;
/*
select * from entities_of_interest;
*/
