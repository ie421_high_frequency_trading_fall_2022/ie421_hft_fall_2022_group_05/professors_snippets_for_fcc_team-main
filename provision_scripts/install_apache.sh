#!/bin/bash

echo "Installing Apache Webserver and associated tools"
#TODO: can probalby cut this down to httpd and maybe a few required plugins
sudo dnf -y group install "Web Server"

#sudo firewall-cmd --add-service=http --add-service=https --permanent
#sudo firewall-cmd --reload

sudo cp /vagrant/provision_files/httpd.conf /etc/httpd/conf/httpd.conf
sudo chmod 644 /etc/httpd/conf/httpd.conf

sudo ln -s /vagrant/google_earth_server/cgi-bin/test_html2.py /var/www/cgi-bin/test_html2.py

sudo systemctl start httpd

