#!/bin/bash

echo "Installing Oracle MySQL package repo"
sudo dnf -y install https://dev.mysql.com/get/mysql80-community-release-fc35-2.noarch.rpm

echo "Installing MySQL 8 Community Server"
sudo dnf -y install mysql-community-server

echo "Configuring MySQL to allow load data infile"
#append secure_file_priv='' to the end of MySQL config file, this will allow mysql to use load data infile commands across the filesystem 
#FIXME: limit this to only the /vagrant/data or other relevant dir
echo "secure_file_priv=''" | sudo tee -a /etc/my.cnf


#install bzip2 for decompressing reference data that tar/bzip2ed from box
sudo yum -y install bzip2


echo "Configuring firewall to allow mysqld 3306 remote access"
#sudo firewall-cmd --new-zone=mysql-access --permanent
#sudo firewall-cmd --reload
#sudo firewall-cmd --zone=mysql-access --add-port=3306/tcp  --permanent
#sudo firewall-cmd --reload
#
#FIXME: find workable settings to allow just traffic into mysql 3306
sudo systemctl stop firewalld
sudo systemctl disable firewalld

#Disable selinux for now to allow mysql user / mysqld server to access the FCC database files use "load data infile"
sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

#FIXME: add to persist; see https://tecadmin.net/how-to-disable-selinux-on-fedora/ will need to modify /etc/selinux/config change to "SELINUX=permissive"


echo "Enabling and starting mysql server"
sudo systemctl start mysqld.service
sudo systemctl enable mysqld.service

#For rationale for below, see https://bertvv.github.io/notes-to-self/2015/11/16/automating-mysql_secure_installation/
#	Note several changes were made by me to allow setting the password to vagrant

echo "Cleaning up security and setting root password to vagrant"
export MYSQLPASS=`sudo grep 'A temporary password' /var/log/mysqld.log |tail - | awk '{print $13}'`

mysql --connect-expired-password --user=root -p$MYSQLPASS <<_EOF_
set password = 'Vagrant123$';
SET GLOBAL validate_password.policy=LOW;
FLUSH PRIVILEGES;
_EOF_

mysql --connect-expired-password --user=root -pVagrant123\$ <<_EOF_
SET GLOBAL validate_password.policy=LOW;
SET GLOBAL validate_password.length=7;
set password = 'vagrant';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';

create user 'root'@'%' identified by 'vagrant';
grant all privileges on *.* to 'root'@'%' with grant option;


FLUSH PRIVILEGES;
_EOF_

echo "Finished configuring MySQL"


#install other packages needed for db
#TODO: move this to a different provision script
sudo dnf install -y python3-pip
python3 -m pip install PyMySQL


