#!/bin/bash

DATA_FOLDER="/vagrant/data"

echo "Generating data"

echo "Generating data folder if it does not exist"
mkdir -p $DATA_FOLDER

echo "Target download dir is $DATA_FOLDER"

if [ ! -f $DATA_FOLDER/EN.dat ]; then
	echo "$DATA_FOLDER/EN.dat does not exist; downloading and generating FCC files"
	wget -O - https://uofi.box.com/shared/static/wdqip849l8g1l887d37p3uiw44nf0u1d | tar -xjf - -C $DATA_FOLDER
	echo "The contents of this file was extracted from 2018_fcc_data.tar.bz2 stored at https://uofi.box.com/shared/static/wdqip849l8g1l887d37p3uiw44nf0u1d" >> $DATA_FOLDER/README.txt
	ls -lA $DATA_FOLDER
fi

echo "Regenerating database"
mysql -u root -pvagrant < /vagrant/sql/create_db_user_and_tables.sql


echo "Finished generating data"


